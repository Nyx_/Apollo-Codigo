#include <SoftwareSerial.h>

#define RxD  0 //Define RxD how 0
#define TxD  1 //Define TxD how 1

SoftwareSerial blueToothSerial(RxD,TxD); //Instancing the SoftwareSerial library

// defining the mutate variables
char controlState;
int userInput;
int apolloSpeed = 200;

// constants variables
const int engineA1 = 7; // Pin  7  Monster.
const int engineA2 = 4; //  Pin  4  Monster.
const int engineB1 = 8; //  Pin   8  Monster.
const int engineB2 = 9; //  Pin   9  Monster.


// functions signature
void conectBluetooth();
int  apolloVelocity(char controlState, int apolloSpeed = 0);
void apolloMovement(char controlState, int apolloSpeed = 0);
void moveForward(int apolloSpeed = 0);
void moveLeft(int apolloSpeed = 0);
void moveRight(int apolloSpeed = 0);
void moveBack(int apolloSpeed = 0);
void stop();


void setup()
{
    connectBluetooth(); 

    pinMode(engineA1, OUTPUT);
    pinMode(engineA2, OUTPUT);

    pinMode(engineB1, OUTPUT);
    pinMode(engineB2, OUTPUT);
    
    Serial.begin(9600);
}


void loop()
{
    while (blueToothSerial.available())
    {
        userInput = blueToothSerial.read();
        controlState = userInput;
    
         // calling the principal functions
        apolloSpeed = apolloVelocity(controlState, apolloSpeed);
        apolloMovement(apolloSpeed, controlState);
    }
    
}


void pairingBluetooth()
{
    
    blueToothSerial.begin(9600); // baud rate do bluetooth  Config in 9600
    blueToothSerial.print("\r\n+STWMOD=0\r\n"); // slave mode config
    blueToothSerial.print("\r\n+STNA=ApolloBluetooth\r\n"); // bluetooth name device config
    blueToothSerial.print("\r\n+STOAUT=1\r\n"); // visible bluetooth
    blueToothSerial.print("\r\n+STAUTO=0\r\n"); // turn of auto connect
    delay(2000); // await 2 segundos
    blueToothSerial.print("\r\n+INQ=1\r\n"); // pairing mode
    delay(2000); // await 2 segundos
    blueToothSerial.flush(); //clean the bluetooth cache
  
}


int apolloVelocity(char controlState, int apolloSpeed = 0)
{
    switch (controlState)
    {
    case '0':
        apolloSpeed = 0;
        break;
    case '4':
        apolloSpeed = 100;
        break;
    case '6':
        apolloSpeed = 155;
        break;
    case '7':
        apolloSpeed = 180;
        break;
    case '8':
        apolloSpeed = 200;
        break;
    case '9':
        apolloSpeed = 230;
        break;
    case 'q':
        apolloSpeed = 255;
        break;
    }

    return apolloSpeed;
}


void moveForward(int apolloSpeed = 0)
{
    analogWrite(engineB1, apolloSpeed);
    analogWrite(engineA1, apolloSpeed);
    analogWrite(engineA2, 0);
    analogWrite(engineB2, 0);
}


void moveLeft(int apolloSpeed = 0)
{
    analogWrite(engineA1, 0);
    analogWrite(engineA2, apolloSpeed);
    analogWrite(engineB1, apolloSpeed);
    analogWrite(engineB2, 0);
}


void moveRight(int apolloSpeed = 0)
{
    analogWrite(engineA1, apolloSpeed);
    analogWrite(engineA2, 0);
    analogWrite(engineB1, 0);
    analogWrite(engineB2, apolloSpeed);
}


void moveBack(int apolloSpeed = 0)
{
    analogWrite(engineA1, 0);
    analogWrite(engineB1, 0);
    analogWrite(engineB2, apolloSpeed);
    analogWrite(engineA2, apolloSpeed);
}


void stop()
{
    analogWrite(engineA1, 0);
    analogWrite(engineA2, 0);
    analogWrite(engineB1, 0);
    analogWrite(engineB2, 0);
}


void apolloMovement(char controlState, int apolloSpeed = 0)
{
    switch (controlState)
    {
    case 'F':
        moveForward(apolloSpeed);
        break;
    case 'L':
        moveLeft(apolloSpeed);
        break;
    case 'R':
        moveRight(apolloSpeed);
        break;
    case 'B':
        moveBack(apolloSpeed);
        break;
    case 'S':
        stop();
        break;
    }
}
